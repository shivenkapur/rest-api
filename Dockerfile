FROM alpine:3.7

EXPOSE 3000

RUN apk update && \
  apk add nodejs bash yarn

RUN adduser -D restapi
USER restapi

# these layers should be fairly static
COPY package.json /home/restapi
RUN export NODE_ENV=production && \
  cd /home/restapi && \
  yarn install

# must run yarn build on the host first
COPY dist /home/restapi/dist
WORKDIR /home/restapi
RUN mkdir images
COPY sample-images/* images/

CMD export NODE_ENV=production && yarn run start
