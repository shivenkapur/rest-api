declare interface TimeStamped {
  createdAt?: Date;
  updatedAt?: Date;
}
