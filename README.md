# OneRelief REST API

## Overview

The OneRelief REST API is a plain Express application. The API is described using Blueprint and data is stored using MongoDB. REST endpoints requiring authentication use Auth0 integration and expects clients to be pre-authenticated.

The following main technologies are used:

* [TypeScript](https://www.typescriptlang.org/docs/home.html)
* [lodash](https://lodash.com)
* [MongoDB](https://docs.mongodb.com/)
* [Mongoose](http://mongoosejs.com/)
* [Node Migrate](https://github.com/tj/node-migrate)
* [Auth0](https://auth0.com/docs)
* [api blueprint](https://apiblueprint.org/)
* [Jest](https://facebook.github.io/jest/)
* [faker](https://marak.github.io/faker.js/)

### Auth0

The API has endpoints that are secured and can only be used by authorised users of the Admin app. The security is provided by Auth0.

The user of the SPA obtains authentication using the Auth0 authentication server. As part of the server's response the client is provided with an `access_token`, typically a JWT. This is an *implicit grant*. This token can then be placed in the authorization header, using the Bearer scheme, of API requests that require authentication. A middleware function in `src/server/secured.ts` can be added to any routes that require authentication.

[This](https://auth0.com/docs/architecture-scenarios/application/spa-api/part-1) example is more involved as it includes the notions of scopes and authority, which is not relevant for the current security model, but illustrates all the concepts.

## Developer Guidelines

The general guidlines in the [PayPal API style guide](https://github.com/paypal/api-standards/blob/master/api-style-guide.md) should be followed.

### API Blueprint

TBD

### Yarn scripts

TBD

## Docker image

### Build

The Docker image `local.Dockerfile` build will install its own dependencies using the host `package.json` file but then copy the `dist` folder from the host into the image. A local production build should be performed prior to building the image. e.g.

`docker build . -f local.Dockerfile -t rest-api`

### Run

To run the Docker image:

* Pass the env variables to the container
* Supply a port mapping for Node.js
* Map the image upload directory to the host so that it can be served
* Make sure the OR_MONGO_URL env setting is reachable from the container

For example, given an image named `restapi`, and the following `.env` file variabless:

```
OR_MONGO_URL=mongodb://127.0.0.1/onerelief
PHOTO_DIR=/home/restapi/images
OR_SECURED=false
```

 A container called `restapi` can be started from an image called `restapi` with the command:

`docker run --net="host" --env-file .env -p 3000:3000 -v /tmp/onerelief-photos/:/home/restapi/images:rw --name restapi restapi`

## Heroku

It is possible to deploy to a free Heroku account using the [mLab MongoDB](https://elements.heroku.com/addons/mongolab) add-on (verification required). Simply install the add-on and add the `MONGO_URL` config var.

The config var `OR_SECURED` can be set to `false` to use secured endpoints and `IMAGE_STORAGE` can be set to `local` to store the uploads directly to disc in the project directory (wiped when Heroku is redeployed).

## Docker Deployment

[Reference](https://devcenter.heroku.com/articles/container-registry-and-runtime)

First log in to the image repository using `heroku container:login`, which will use existing Heroku credentials.

The image can be built and pushed to the repository using the Heroku cli tool: `heroku container:push web`, but this will only look for Docker files named `Dockerfile`. Alternatively, use the script `push-image-heroku.sh`.
