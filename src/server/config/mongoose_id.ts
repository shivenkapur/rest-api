import { Schema, SchemaOptions } from 'mongoose';

// Use id in JSON output instead of _id
function transformId(doc: any, ret: any, options: any) {
  // remove the _id of every document before returning the result
  ret.id = ret._id;
  delete ret._id;
  delete ret.__v;
}

export default (schema: Schema, options: SchemaOptions) => {
  schema.set('toJSON', {
    transform: transformId
  });
};
