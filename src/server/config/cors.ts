import * as cors from 'cors';
import { Application } from 'express';

const corsList = process.env.OR_CORS_DOMAINS ? process.env.OR_CORS_DOMAINS : '';
const whitelist: string[] = corsList.split(',');

let originOption;
if (process.env.NODE_ENV === 'production') {
  originOption = (origin: string, callback: any) => {
    // origin undefined if request is not cross origin
    if (
      origin === undefined ||
      whitelist.some((val: string, idx: number) => origin.endsWith(val))
    ) {
      callback(null, true);
    } else {
      callback(new Error(`site ${origin} is not in CORS whitelist`));
    }
  };
} else {
  originOption = '*';
}

const options = {
  origin: originOption,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

function configure(app: Application) {
  app.use(cors(options));
}

export default configure;
