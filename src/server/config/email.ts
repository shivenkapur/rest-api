import * as nodemailer from 'nodemailer';
import * as aws from 'aws-sdk';
// tslint:disable-next-line
import { SmtpOptions } from 'nodemailer-smtp-transport';
import * as Email from 'email-templates';

// fix-up lack of predefined service fields on TS definition
declare interface SESSmtpOptions extends SmtpOptions {
  SES: aws.SES;
}

type EmailService = 'mailgun' | 'ses' | 'json';

const service: EmailService = process.env.OR_EMAIL_SERVICE as EmailService;

let transport: nodemailer.Transporter | { jsonTransport: boolean };

switch (service) {
  case 'mailgun':
    transport = nodemailer.createTransport({
      service: 'Mailgun',
      auth: {
        user: process.env.OR_SMTP_USER,
        pass: process.env.OR_SMTP_PASSWORD
      }
    });
    break;
  case 'ses':
    const awsOptions: SESSmtpOptions = {
      SES: new aws.SES({
        apiVersion: '2010-12-01'
      })
    };
    transport = nodemailer.createTransport(awsOptions);
    break;
  case 'json':
    transport = {
      jsonTransport: true
    };
    break;
  default:
    throw Error('invalid OR_EMAIL_SERVICE');
}

export { transport };
