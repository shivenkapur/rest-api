import * as mongoose from 'mongoose';
import transformId from './mongoose_id';

(mongoose as any).Promise = global.Promise;
mongoose.plugin(transformId);

const connect = async () => {
  const env = process.env.NODE_ENV || 'development';

  if (env === 'testing') {
    const url = 'mongodb://onerelief.com/testing';
    console.log(`Connecting to Mongoose DB ${url} ...`);

    await global.__MONGO__.getConnectionString().then((mongoUri: string) => {
      const mongooseOpts = {
        autoReconnect: true,
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000
      };
      mongoose.connect(
        mongoUri,
        mongooseOpts
      );
    });
  } else {
    const dbUrl = process.env.OR_MONGO_URL;
    if (!dbUrl) {
      throw new Error('OR_MONGO_URL is undefined');
    }
    console.log(`Conecting to Mongoose DB ${dbUrl} ...`);
    await mongoose
      .connect(
        dbUrl,
        {}
      )
      .then(() => {
        console.log('Mongoose connected');
      })
      .catch(reason => {
        throw reason;
      });
  }
};

export { connect, mongoose };
