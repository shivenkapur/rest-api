import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as path from 'path';

const defaultPath = path.join(process.cwd(), '/.env');

if (fs.existsSync(defaultPath)) {
  console.log('found .env file, values will be used if missing from shell');
  const { error } = dotenv.config({ path: '.env' });
  if (error) {
    throw new Error('unable to parse .env file');
  }
} else {
  console.log(
    'ignoring missing .env file, values will be taken from shell env'
  );
}
