const onerelief = {
  telephone: '+1 (240) 389-4057',
  addressOneLine: 'OneRelief | 8112 SE Yamhill St | 97215 Portland, Oregon',
  nameCharityNum: 'OneRelief 501(c)(3) #82-3354121',
  logoUrl:
    'https://onereliefapp.com/wp-content/uploads/2017/11/logo-full-120.png'
};

export { onerelief };
