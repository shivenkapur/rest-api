import { CronJob } from 'cron';
import { updateAllDonationTotals } from '../modules/campaigintotals';
import { processEmails } from '../modules/emailprocessing';

function createDonationTotalsJob(startNow: boolean = false) {
  return new CronJob(
    // '00 00 00 * * 1-7',
    // run every 15 seconds instead of at midnight
    '*/15 * * * * *',
    () => {
      // every day at midnight
      updateAllDonationTotals().catch(err => {
        console.error(err);
        // TODO ring alarm bells
      });
    },
    () => {
      console.log('CronJob: Finished updating campaign donations totals');
    },
    startNow,
    // timezone
    'America/New_York'
  );
}

function createEmailerJob(startNow: boolean = false) {
  return new CronJob(
    '00 * * * * 1-7',
    () => {
      // every minute
      processEmails();
    },
    () => {
      console.log('CronJob: Finished processing emails');
    },
    startNow,
    // timezone
    'America/New_York'
  );
}
function createJobs() {
  createDonationTotalsJob(true);
  createEmailerJob(true);
}

export { createJobs, createDonationTotalsJob, createEmailerJob };
