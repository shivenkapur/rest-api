import * as express from 'express';
import { json, urlencoded, raw } from 'body-parser';
import * as path from 'path';

import cors from './config/cors';
import { createJobs } from './config/cron';

import { router as charityRouter } from './routes/v1/charity';
import { router as campaignRouter } from './routes/v1/campaign';
import { router as photosRouter } from './routes/v1/photos';
import { router as donationsRouter } from './routes/v1/donations';
import { router as userPhotosRouter } from './routes/v1/userPhotos';


console.log(`NODE_ENV=${process.env.NODE_ENV}`);

createJobs();

const app: express.Application = express();
app.disable('x-powered-by');
cors(app);

app.use(json());
app.use(
  raw({
    limit: '5mb',
    type: ['application/octet-stream', 'image/png', 'image/jpg', 'image/jpeg']
  })
);

app.use(
  urlencoded({
    extended: true
  })
);

app.get('/', (req: express.Request, resp: express.Response) => {
  resp.json({
    name: 'OneRelief REST API'
  });
});

app.use('/api/v1', charityRouter);
app.use('/api/v1', campaignRouter);
app.use('/api/v1', photosRouter);
app.use('/api/v1', donationsRouter);
app.use('/api/v1', userPhotosRouter);
app.use('/docs', express.static(path.join(__dirname, './assets/blueprint')));

// if not using S3 storage need to serve the uploaded local images from Express
if (process.env.OR_IMAGE_STORAGE === 'local') {
  app.use('/images', express.static(path.join(process.cwd(), 'images')));
}

// error handler must come after route handlers
app.use(
  (
    err: Error & { status: number },
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ): void => {
    if (res.headersSent) {
      return next(err);
    }
    console.error(err);
    res.status(err.status || 500);
    res.json({
      error: 'Server Error'
    });
  }
);
const portNum = process.env.PORT ? +process.env.PORT : 3000;

// promisify the Express listen function
const appListen = (port: number) => {
  return new Promise((resolve, reject) => {
    const server = app.listen(port, (err: any) => {
      if (err) {
        reject(err);
      } else {
        process.env.PORT = `${port}`;
        resolve(server);
      }
    });
  });
};

const start = () => {
  return appListen(portNum).then(server => {
    console.log(
      `App is running at http://localhost:${portNum} in ${app.get('env')} mode`
    );
    console.log('Press CTRL-C to stop\n');

    return server;
  });
};

export { app, start };
