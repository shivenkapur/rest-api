import * as jwt from 'express-jwt';
import * as jwks from 'jwks-rsa';
import * as ip from 'ip';
import * as os from 'os';
import { Request, Response, Handler, NextFunction } from 'express';

function localAddresses(): string[][] {
  const ifaces = os.networkInterfaces();
  // https://stackoverflow.com/questions/3653065/get-local-ip-address-in-node-js
  const cidrs: string[][] = [];
  Object.keys(ifaces).forEach(ifname => {
    ifaces[ifname].forEach(iface => {
      if (iface.netmask) {
        cidrs.push([iface.address, iface.netmask]);
      }
    });
  });
  console.log(`found local interface addresses/subnets: ${cidrs}`);
  return cidrs;
}

const ifAddressAndSubnets = localAddresses();

const securityOn: boolean =
  process.env.OR_SECURED !== undefined
    ? process.env.OR_SECURED === 'true'
    : true;

if (!securityOn) {
  console.warn('Running without secure endpoints!');
}

function inSubnet(req: Request, resp: Response, next: NextFunction) {
  const mappedPrefix = '::ffff:';
  let ipAddress = req.connection.remoteAddress
    ? req.connection.remoteAddress
    : '';
  if (ipAddress.startsWith(mappedPrefix)) {
    ipAddress = ipAddress.slice(mappedPrefix.length, ipAddress.length);
  }
  console.log(`checking subnet trust on IP address: ${ipAddress}`);
  let trusted = false;
  ifAddressAndSubnets.forEach(addressSubnet => {
    const subnet = ip.subnet(addressSubnet[0], addressSubnet[1]);
    if (subnet.contains(ipAddress)) {
      trusted = true;
    }
  });
  if (trusted) {
    next();
  } else {
    throw Error('invalid IP address');
  }
}

const doNothing = (req: Request, resp: Response, next: NextFunction) => next();

// using security of source ip address. would prefer to use auth0 machine-to-machine security
// but this is not on the free plan.
const subnetSecured = securityOn ? inSubnet : doNothing;

const auth0Secured = securityOn
  ? jwt({
      secret: jwks.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: 'https://onerelief.auth0.com/.well-known/jwks.json'
      }),
      audience: 'https://onereliefapp.com/api',
      issuer: 'https://onerelief.auth0.com/',
      algorithms: ['RS256']
    })
  : doNothing;

export { auth0Secured, subnetSecured };
