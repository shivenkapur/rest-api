import { mongoose } from '../config/database';
import { Schema, Document, Model, SchemaOptions } from 'mongoose';

import { url } from '../modules/storage';

declare interface Photo extends Document {
  id?: string;
  fileName: string;
  url?: string;
}
interface PhotoModel extends Model<Photo> {}

const schema = new Schema({
  fileName: {
    type: String,
    required: true
  }
});

const model = mongoose.model<Photo>('Photo', schema) as PhotoModel;

(function addJsonTransform() {
  function addUrl(doc: any, ret: any, options: SchemaOptions) {
    ret.url = url(ret.fileName);
  }

  const { transform } = model.schema.get('toJSON');

  const toJSON = (doc: any, ret: any, options: SchemaOptions) => {
    transform(doc, ret, options);
    addUrl(doc, ret, options);
  };

  model.schema.set('toJSON', {
    transform: toJSON
  });
})();
export { model, schema, Photo };
