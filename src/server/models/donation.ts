import { mongoose } from '../config/database';
import { Document, Model, Schema } from 'mongoose';

interface Donation extends Document {
  id?: string;
  amount: number;
  paymentMethod: string;
  campaign: Schema.Types.ObjectId;
  paymentId: string;
  donorDetails: {
    email: string;
    name: string;
    address?: string;
    country?: string;
  };
  madeAt: Date;
}

export interface DonationModel extends Model<Donation> {}

const schema = new Schema({
  amount: {
    type: Number,
    required: true
  },
  paymentMethod: {
    type: String,
    required: true
  },
  campaign: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'Campaign'
  },
  paymentId: {
    required: true,
    type: String
  },
  donorDetails: {
    email: {
      type: String,
      required: true
    },
    name: {
      type: String,
      required: true
    },
    address: {
      type: String,
      required: false
    },
    country: {
      type: String,
      required: false
    }
  },
  madeAt: { type: Date, required: true, index: true }
});

const model = mongoose.model<Donation>('Donation', schema) as DonationModel;

export { model, schema, Donation };
