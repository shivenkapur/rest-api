import { mongoose } from '../config/database';
import { Schema, Document, Model } from 'mongoose';

enum EmailStatus {
  New = 1,
  Pending,
  Sent,
  Error
}

declare interface Email extends Document {
  id?: string;
  recipientAddress: string;
  recipientName?: string;
  templateData: Map<string, string>;
  templateName: string;
  status: EmailStatus;
  error?: string;
}
interface EmailModel extends Model<Email> {}

const schema = new Schema(
  {
    recipientAddress: {
      type: String,
      required: true
    },
    recipientName: {
      type: String,
      required: false
    },
    templateName: {
      type: String,
      required: true
    },
    templateData: {
      type: Map,
      of: String,
      required: true
    },
    status: {
      type: String,
      required: true
    },
    error: {
      type: String,
      required: false
    }
  },
  {
    timestamps: true
  }
);

const model = mongoose.model<Email>('Email', schema) as EmailModel;

export { model, schema, Email, EmailStatus };
