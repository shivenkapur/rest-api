import { mongoose } from '../config/database';
import { Schema, Document, Model, SchemaOptions } from 'mongoose';

declare interface UserPhoto extends Document {
  donationId?: string;
  url: string;
  fileName?: string;
  timestamp: Date;
}

interface SocialPhoto extends Model<UserPhoto> {}

const schema = new Schema({
  fileName: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  },
  donationId: {
    type: String,
    require: true
  }
});

schema.pre('save', function(this: any, next) {
  if (!this.timestamp) {
    this.timestamp = new Date();
  }
  next();
});

const model = mongoose.model<UserPhoto>('UserPhoto', schema) as SocialPhoto;

export { model, schema, UserPhoto };
