import { Document, Model, Schema, SchemaOptions } from 'mongoose';

import { mongoose } from '../config/database';
import { Photo, schema as photoSchema } from '../models/photo';

interface Campaign extends Document, TimeStamped {
  _doc: any; // TODO remove
  id?: string;
  title: string;
  active: boolean;
  endDate: Date;
  charity: Schema.Types.ObjectId | string;
  details: {
    images: Map<string, Photo>;
    summary: string;
    description: string;
    areaAffected: string;
    peopleAffected: string;
    helpRequired: string;
    location: {
      lat: number;
      lng: number;
      zoom: number;
      radius: number;
    };
  };
  fundraising: {
    goal: {
      currency: string;
      value: number;
    };
    donations: number;
    amountRaised: number;
  };
}

interface CampaignModel extends Model<Campaign> {}

const schema = new Schema(
  {
    title: {
      type: String,
      required: true
    },
    active: {
      type: Boolean,
      default: false
    },
    charity: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: 'Charity'
    },
    endDate: {
      type: Date,
      required: true
    },
    fundraising: {
      goal: {
        currency: {
          type: String,
          default: 'USD'
        },
        value: {
          type: Number,
          required: true,
          min: 1
        }
      },
      donations: {
        type: Number,
        default: 0,
        min: 0
      },
      amountRaised: {
        type: Number,
        default: 0,
        min: 0
      }
    },

    details: {
      images: {
        type: Map,
        of: photoSchema,
        required: true
      },
      summary: {
        type: String,
        required: true
      },
      description: {
        type: String,
        required: true
      },
      areaAffected: {
        type: String,
        required: true
      },
      peopleAffected: {
        type: String,
        required: true
      },
      helpRequired: {
        type: String,
        required: true
      },
      location: {
        lat: {
          type: Number,
          min: -90,
          max: 90
        },
        lng: {
          type: Number,
          min: -180,
          max: 180
        },
        zoom: {
          type: Number,
          min: 0
        },
        radius: {
          type: Number,
          min: 0
        }
      }
    }
  },
  {
    timestamps: true
  }
);

const model = mongoose.model<Campaign>('Campaign', schema) as CampaignModel;

(function addJsonTransform() {
  function convertImageMap(doc: any, ret: any, options: SchemaOptions) {
    const images: any = {};
    doc.details.images.forEach((val: Photo, key: string) => {
      images[key] = val;
    });
    ret.details.images = images;
  }

  const { transform } = model.schema.get('toJSON');

  const toJSON = (doc: any, ret: any, options: SchemaOptions) => {
    transform(doc, ret, options);
    convertImageMap(doc, ret, options);
  };

  model.schema.set('toJSON', {
    transform: toJSON
  });
})();

export { model, schema, Campaign };
