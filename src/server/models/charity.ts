import { Schema, Document, Model, SchemaOptions } from 'mongoose';

import { mongoose } from '../config/database';
import { Photo, schema as photoSchema } from './photo';
import { url } from '../modules/storage';

declare interface Charity extends Document, TimeStamped {
  id?: string;
  name: string;
  description: string;
  logo: Photo;
}

interface CharityModel extends Model<Charity> {
  // updateCharity(id: {}, ...props): Promise<{ nModified: number }>;
}

const schema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    logo: {
      type: photoSchema,
      required: true
    }
  },
  {
    timestamps: true
  }
);

const model = mongoose.model<Charity>('Charity', schema) as CharityModel;

export { model, schema, Charity };
