import '../server/config/dotenv';
import { start } from './app';
import { connect } from './config/database';

connect()
  .then(() => {
    return start();
  })
  .catch(err => {
    console.error(err);
  });
