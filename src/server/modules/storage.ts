import {
  storeFile as s3StoreFile,
  deleteFile as s3DeleteFile
} from './s3storage';
import {
  storeFile as localStoreFile,
  deleteFile as localDeleteFile
} from './localstorage';

const { storeFile, deleteFile } =
  process.env.OR_IMAGE_STORAGE === 's3'
    ? { storeFile: s3StoreFile, deleteFile: s3DeleteFile }
    : { storeFile: localStoreFile, deleteFile: localDeleteFile };

const url = process.env.OR_IMAGE_STORAGE === 's3' ? s3Url : localUrl;

function localUrl(fileName: string) {
  if (process.env.OR_APP_URL) {
    return `${process.env.OR_APP_URL}/images/${fileName}`;
  }
  return `http://localhost:${process.env.PORT}/images/${fileName}`;
}

function s3Url(filename: string) {
  return `https://s3.amazonaws.com/${
    process.env.OR_AWS_IMAGE_BUCKET
  }/${filename}`;
}

export { storeFile, deleteFile, url };
