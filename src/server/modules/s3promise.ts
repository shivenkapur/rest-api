import * as AWS from 'aws-sdk';

const s3 = new AWS.S3();

const createBucket = (bucketKey: string) => {
  return new Promise((resolve, reject) => {
    s3.createBucket({ Bucket: bucketKey }, (err, data) => {
      if (!err) {
        resolve(data);
      } else {
        reject(err);
      }
    });
  });
};

const putObject = (params: any) => {
  return new Promise((resolve, reject) => {
    s3.putObject(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};

const deleteObject = (params: any) => {
  return new Promise<void>((resolve, reject) => {
    s3.deleteObject(params, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

export { createBucket, putObject, deleteObject };
