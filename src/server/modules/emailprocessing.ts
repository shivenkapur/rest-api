import { model as EmailModel, EmailStatus, Email } from '../models/email';
import { sendEmail } from './email';

async function processEmails() {
  let more = true;
  let processed = 0;
  while (more) {
    let res;
    try {
      res = await processNextEmail();
    } catch (e) {
      console.error(
        'unexpected error while running email cron job, terminating job'
      );
      console.error(e);
      return;
    }
    more = res.more;
    processed += res.processed ? 1 : 0;
  }
  return processed;
}

/**
 * processNextEmail will query for an email with status 'New'. If one is found, an attempt will be made
 * update its status to 'Pending', which indicates that this Node process has 'locked' the Email for processing.
 * If succssfull the Email is then processed and the status updated to 'Sent'.
 * The return value of processNextEmail indicates whether an Email with status 'New' was found,
 * and not whether is was successfully locked and processed.
 */
async function processNextEmail(): Promise<{
  more: boolean;
  processed: boolean;
}> {
  const email: Email | null = await EmailModel.findOne({
    status: EmailStatus.New
  });
  if (!email) {
    return { more: false, processed: false };
  }

  const query = { _id: email.id, status: EmailStatus.New };
  const res = await EmailModel.updateOne(query, {
    status: EmailStatus.Pending
  });
  // check the raw mongo result object for 'lock'
  if (res.n !== 1 || res.nModified !== 1) {
    return { more: true, processed: false };
  }
  try {
    // new email in Pending status
    await processEmail(email);
  } catch (error) {
    console.error(error);
    await EmailModel.update(
      { _id: email.id },
      { status: EmailStatus.Error, error }
    );
    return { more: true, processed: false };
  }
  await EmailModel.update({ _id: email.id }, { status: EmailStatus.Sent });
  return { more: true, processed: true };
}

function processEmail(email: Email) {
  const templateData: any = {};
  email.templateData.forEach((val, key) => {
    templateData[key] = val;
  });
  return sendEmail(email.recipientAddress, email.templateName, templateData);
}

export { processEmails };
