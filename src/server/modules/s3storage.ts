import { putObject, deleteObject } from './s3promise';

const bucket = process.env.OR_AWS_IMAGE_BUCKET || 'one-relief.org';

const storeFile = (fileName: string, body: any) => {
  const params = {
    Bucket: bucket,
    Key: fileName,
    Body: body,
    ACL: 'public-read'
  };
  return putObject(params);
};

const deleteFile = (fileName: string) => {
  const params = { Bucket: bucket, Key: fileName };
  return deleteObject(params);
};

export { storeFile, deleteFile };
