import { model as DonationModel, Donation } from '../models/donation';
import { model as CampaignModel, Campaign } from '../models/campaign';
import { model as CharityModel, Charity } from '../models/charity';
import { model as EmailModel, EmailStatus, Email } from '../models/email';

import * as moment from 'moment';

async function saveDonorEmailConfirmation(
  campaign: Campaign,
  donation: Donation
) {
  return CharityModel.findOne({ _id: campaign.charity }).then(charity => {
    if (!charity) {
      return Promise.reject('Charity not found');
    }

    const email: Email = new EmailModel();
    email.recipientName = donation.donorDetails.name;
    email.recipientAddress = donation.donorDetails.email;
    email.status = EmailStatus.New;
    email.templateName = 'receipt';

    // include this donation
    const amountRaised = campaign.fundraising.amountRaised + donation.amount;
    const numberOfDonors = campaign.fundraising.donations + 1;

    const templateData = new Map();
    templateData.set(
      'donationDate',
      moment(donation.madeAt).format('MM/DD/YY')
    );
    templateData.set('donationAmount', donation.amount.toFixed(2));
    templateData.set('donorName', donation.donorDetails.name);
    templateData.set('donorEmail', donation.donorDetails.email);
    templateData.set('charityName', charity.name);
    templateData.set(
      'campaignGoal',
      campaign.fundraising.goal.value.toFixed(2)
    );
    templateData.set('campaignTitle', campaign.title);
    templateData.set('campaignDisasterArea', campaign.details.areaAffected);
    templateData.set('campaignAmountRaised', amountRaised.toFixed(2));
    templateData.set('campaignNumberDonors', numberOfDonors.toString());
    templateData.set('paymentMethod', donation.paymentMethod);
    templateData.set('paymentID', donation.paymentId);

    const donationsRequired =
      (campaign.fundraising.goal.value - amountRaised) /
      (amountRaised / numberOfDonors);

    templateData.set('donationsRequired', donationsRequired.toFixed(0));
    email.templateData = templateData;

    return email.save();
  });
}

export { saveDonorEmailConfirmation };
