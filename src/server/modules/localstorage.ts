import { promisify } from 'util';
import * as path from 'path';
import * as fs from 'fs';

const imagePath = 'images';

function photoPath(file: string) {
  return path.join(imagePath, file);
}

const storeFile = (fileName: string, body: any) => {
  return promisify(fs.open)(photoPath(fileName), 'w').then(fd => {
    return promisify(fs.write)(fd, body);
  });
};

const deleteFile = (fileName: string) => {
  return promisify(fs.unlink)(photoPath(fileName));
};

export { imagePath, storeFile, deleteFile };
