import * as Email from 'email-templates';
import * as path from 'path';
import { onerelief } from '../config/onerelief';

import { transport } from '../config/email';

function sendEmail(recipient: string, template: string, data: any) {
  const email = new Email({
    views: { root: path.join(__dirname, '../templates') },
    message: {
      from: `${process.env.OR_CONTACT_EMAIL}`
    },
    send: true,
    transport
  });
  return email.send({
    template,
    message: {
      to: recipient
    },
    locals: { data, onerelief }
  });
}

export { sendEmail };
