import { model as DonationModel } from '../models/donation';
import { model as CampaignModel } from '../models/campaign';

async function updateDonationTotals(campaignId: any) {
  const campaign = await CampaignModel.findById(campaignId);
  if (!campaign) {
    throw new Error('invalid campaignId');
  }
  let numDonors: number = 0;
  let amountRaised: number = 0;

  const donations = await DonationModel.find({ campaign: campaignId })
    .cursor()
    .eachAsync(donation => {
      amountRaised += donation.amount;
      numDonors += 1;
    });

  await campaign.update({
    fundraising: { ...campaign.fundraising, amountRaised, donations: numDonors }
  });
}

async function updateAllDonationTotals() {
  const campaigns = await CampaignModel.find();
  return Promise.all(
    campaigns.map(campaign => {
      return updateDonationTotals(campaign._id);
    })
  );
}

export { updateAllDonationTotals, updateDonationTotals };
