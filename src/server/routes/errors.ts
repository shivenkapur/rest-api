const errors = {
  invalidField: {
    code: 'Invalid Field',
    message: 'There is a problem with this field value'
  }
};

export default errors;
