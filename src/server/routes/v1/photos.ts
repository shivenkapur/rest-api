import { Router, Request, Response } from 'express';

import * as imageType from 'image-type';

import { model as photoModel, Photo } from '../../models/photo';
import { auth0Secured as secured } from '../../secured';
import { uuid } from '../../util';
import { storeFile, deleteFile } from '../../modules/storage';

const router = Router();

router.post('/photos', secured, (req: Request, resp: Response) => {
  const fileInfo = imageType(req.body);
  if (!fileInfo || fileInfo.mime.indexOf('image') !== 0) {
    console.error('file is not a recognized image type', req.body);
    // TODO send json validation error
    resp.status(400).send();
    return;
  }
  const fileName = `${uuid()}.${fileInfo.ext}`;
  storeFile(fileName, req.body)
    .then(res => {
      return photoModel.create({ fileName });
    })
    .then(model => {
      resp.status(201).json(model.toJSON());
    })
    .catch(err => {
      console.error('unable to store photo', err);
      resp.sendStatus(500);
    });
});

router.delete('/photos/:id', secured, (req: Request, resp: Response) => {
  const id = req.params.id;
  let found = false;
  let fileName: string;
  photoModel
    .findById(id)
    .then(res => {
      if (!res) {
        throw Error('id not found');
      }
      found = true;
      fileName = res.fileName;
      return photoModel.deleteOne({ _id: id }).exec();
    })
    .then(res => {
      return deleteFile(fileName);
    })
    .then(res => {
      resp.send();
    })
    .catch(err => {
      if (!found) {
        resp.sendStatus(404);
      } else {
        console.error(err);
        resp.sendStatus(500);
      }
    });
});

export { router };
