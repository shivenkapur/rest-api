import { Router, Request, Response } from 'express';
import { model as userPhotoModel } from '../../models/userPhoto';
import { mongooseValidation } from '../format';

const router = Router();

export interface MulterFile {
  key?: string;
  location?: string;
  mimetype?: string;
}

router.post(
  '/userphotos',
  (req: Request & { file: MulterFile }, resp: Response) => {
    userPhotoModel
      .create({
        fileName: req.body.fileName,
        url: req.body.url,
        donationId: req.body.donationId
      })
      .then(photo => {
        resp.status(201).json(photo);
      })
      .catch(err => {
        if (err.errors) {
          console.log(err.errors);
          resp.status(400);
          resp.json(mongooseValidation(err.errors));
          return;
        }
        resp.status(500).send();
      });
  }
);

export { router };
