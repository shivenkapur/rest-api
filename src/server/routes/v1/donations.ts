import { Router, Request, Response } from 'express';
import * as _ from 'lodash';
import { auth0Secured, subnetSecured } from '../../secured';
import { Schema } from 'mongoose';
// tslint:disable-next-line:no-submodule-imports
import { check, validationResult } from 'express-validator/check';
import * as moment from 'moment';

import { mongooseValidation, mongooseErrorOr404 } from '../format';
import { model as DonationModel, Donation } from '../../models/donation';
import { model as CampaignModel, Campaign } from '../../models/campaign';
import { model as CharityModel, Charity } from '../../models/charity';
import { model as EmailModel, EmailStatus } from '../../models/email';
import { saveDonorEmailConfirmation } from '../../modules/donoremailconfirmation';
const router = Router();

const defaultPageSize = 1000;

router.post(
  '/campaigns/:id/donations',
  subnetSecured,
  (req: Request, resp: Response) => {
    let campaign: Campaign;
    let donation: Donation;
    CampaignModel.findOne({ _id: req.params.id })
      .then(doc => {
        if (!doc) {
          resp.status(404).send();
          return;
        }
        campaign = doc;

        const model = new DonationModel(req.body);
        model.campaign = req.params.id;
        return model.save();
      })
      .then(saved => {
        donation = saved;
        return saveDonorEmailConfirmation(campaign, saved);
      })
      .then(() => {
        resp.status(201);
        resp.json(donation);
      })
      .catch(err => {
        if (err.errors) {
          resp.status(400);
          resp.json(mongooseValidation(err.errors));
          return;
        }
        mongooseErrorOr404(err, resp);
      });
  }
);

function isUnixMilliseconds(val: any) {
  return moment.unix(val).isValid();
}
const donationQueryValidator = [
  check('dateStart')
    .optional()
    .custom(isUnixMilliseconds),
  check('dateEnd')
    .optional()
    .custom(isUnixMilliseconds),
  check('amountMin')
    .optional()
    .isInt(),
  check('amountMax')
    .optional()
    .isInt(),
  check('count')
    .optional()
    .isInt(),
  check('startId')
    .optional()
    .custom(isUnixMilliseconds),
  check('name')
    .optional()
    .isLength({ min: 3 })
    .withMessage('name should have a minimum of 3 characters')
];

router.get(
  '/donations',
  donationQueryValidator,
  (req: Request, resp: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return resp.status(400).json({ errors: errors.mapped() });
    }

    const query = req.query;
    let docQuery = DonationModel.find();

    if (query.dateEnd) {
      docQuery.lt('madeAt', +query.dateEnd);
    }
    if (query.dateStart) {
      docQuery.gt('madeAt', +query.dateStart);
    }
    if (query.amountMin) {
      docQuery = docQuery.gte('amount', +query.amountMin);
    }
    if (query.amountMax) {
      docQuery = docQuery.lte('amount', +query.amountMax);
    }
    if (query.name) {
      const reg = new RegExp(`${_.escapeRegExp(query.name)}`);
      docQuery = docQuery.regex('donorDetails.name', reg);
    }
    docQuery = docQuery.sort('-madeAt');
    if (query.startId) {
      docQuery = docQuery.lte('madeAt', +query.startId);
    }
    const limit = query.count ? +query.count : defaultPageSize;
    docQuery = docQuery.limit(limit + 1);

    docQuery
      .then(donations => {
        const more = donations.length === limit + 1;
        const json: any = {};
        if (more) {
          json.value = donations.slice(0, donations.length - 1);
          const lastItem = donations[donations.length - 1];
          json.nextId = lastItem.madeAt.getTime();
        } else {
          json.value = donations;
        }
        resp.json(json);
      })
      .catch(err => {
        console.error(err);
        resp.send(500);
      });
  }
);

router.get('/donations/:id', (req: Request, resp: Response) => {
  DonationModel.findOne({ _id: req.params.id })
    .then(donation => {
      if (!donation) {
        resp.status(404).send();
        return;
      }
      resp.json(donation);
    })
    .catch(err => {
      console.log(err);
      mongooseErrorOr404(err, resp);
    });
});

router.post('/donations/getId', (req: Request, resp: Response) => {
  const query = DonationModel.findOne({ paymentId: req.body.id });
  const result = query.exec();
  return result
    .then(donation => {
      if (!donation) {
        resp.status(404).send();
        return;
      }
      resp.json(donation);
    })
    .catch(err => {
      console.log(err);
      mongooseErrorOr404(err, resp);
    });
});

export { router };
