import { Router, Request, Response } from 'express';
import { model as charityModel, Charity } from '../../models/charity';
import { auth0Secured } from '../../secured';
import { mongooseValidation, mongooseErrorOr404 } from '../format';

const router = Router();

router.post('/charities', auth0Secured, (req: Request, resp: Response) => {
  charityModel
    .create(req.body)
    .then(charity => {
      resp.status(201).json(charity);
    })
    .catch(err => {
      if (err.errors) {
        resp.status(400);
        resp.json(mongooseValidation(err.errors));
        return;
      }
      resp.status(500).send();
    });
});

router.patch('/charities/:id', auth0Secured, (req: Request, resp: Response) => {
  const charityId = req.params.id;
  charityModel
    .findOneAndUpdate({ _id: charityId }, req.body)
    .then((charity: Charity | null) => {
      if (!charity) {
        resp.status(404).send();
        return;
      }
      return charityModel.findOne({ _id: charityId }).exec();
    })
    .then((updated: Charity | null) => {
      if (updated) {
        resp.json(updated);
      }
    })
    .catch(err => {
      if (err.errors) {
        resp.status(400);
        resp.json(mongooseValidation(err.errors));
        return;
      }
      mongooseErrorOr404(err, resp);
    });
});

router.get('/charities/:id', (req: Request, resp: Response) => {
  charityModel
    .findOne({ _id: req.params.id })
    .then(charity => {
      if (!charity) {
        resp.status(404).send();
        return;
      }
      resp.json(charity);
    })
    .catch(err => {
      console.error(err);
      mongooseErrorOr404(err, resp);
    });
});

router.get('/charities', (req: Request, resp: Response) => {
  charityModel
    .find()
    .then(charities => {
      resp.json(charities);
    })
    .catch(err => {
      resp.status(500).send();
    });
});

export { router };
