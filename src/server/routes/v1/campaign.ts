import { Router, Request, Response } from 'express';

import { model as CampaignModel, Campaign } from '../../models/campaign';
import { model as DonationModel } from '../../models/donation';
import { mongooseValidation, mongooseErrorOr404 } from '../format';
import { auth0Secured } from '../../secured';

const router = Router();

router.post('/campaigns', auth0Secured, (req: Request, resp: Response) => {
  const campaign = new CampaignModel(req.body);

  campaign
    .save()
    .then((doc: Campaign) => {
      resp.status(201).json(doc);
    })
    .catch(err => {
      if (err.errors) {
        resp.status(400);
        resp.json(mongooseValidation(err.errors));
        return;
      }
      console.error('Error creating the campaign', err);
      resp.status(500).send();
    });
});

router.get('/campaigns', (req: Request, resp: Response) => {
  let find = CampaignModel.find();
  const criteria = req.query.criteria;
  const inactive = req.query.inactive === 'true';

  if (criteria) {
    find = find.where('title').regex(new RegExp(`${criteria}`));
  }
  if (!inactive) {
    find = find.where('active').equals(true);
  }
  find
    .sort('endDate')
    .populate('charity')
    .then(campaigns => {
      resp.json({ items: campaigns });
      return;
    })
    .catch(error => {
      console.error(error);
      resp.status(500).send();
    });
});

router.get('/campaigns/:id', (req: Request, resp: Response) => {
  CampaignModel.findOne({ _id: req.params.id })
    .populate('charity')
    .then(campaign => {
      if (campaign == null) {
        resp.status(404).send();
        return;
      }
      resp.json(campaign);
    })
    .catch(error => {
      console.error(error);
      mongooseErrorOr404(error, resp);
    });
});

router.put('/campaigns/:id', auth0Secured, (req: Request, resp: Response) => {
  const campaignId = req.params.id;
  CampaignModel.findOneAndUpdate({ _id: req.params.id }, req.body, {
    runValidators: true
  })
    .then(campaign => {
      if (campaign == null) {
        resp.status(404).send();
        return;
      }
      return CampaignModel.findOne({ _id: campaignId })
        .populate('charity')
        .exec();
    })
    .then(campaign => {
      resp.status(200);
      resp.send(campaign);
    })
    .catch(err => {
      console.log(err);
      if (err.errors) {
        resp.status(400);
        resp.json(mongooseValidation(err.errors));
        return;
      }
      mongooseErrorOr404(err, resp);
    });
});

router.delete(
  '/campaigns/:id',
  auth0Secured,
  async (req: Request, res: Response) => {
    try {
      const campaign = await CampaignModel.findById(req.params.id);
      if (!campaign) {
        res.status(404).send();
        return;
      }
      if (campaign.active) {
        res.status(400).send();
        return;
      }
      const donations = await DonationModel.find({ campaign: campaign._id });
      if (donations.length > 0) {
        res.status(400).send();
        return;
      }
      await campaign.remove();
      res.status(202).send();
    } catch (err) {
      console.error(err);
      res.status(500).send();
    }
  }
);

export { router };
