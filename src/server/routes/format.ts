import e from './errors';
import { ValidationError } from 'mongoose';

function mongooseValidation(validationErrors: any) {
  const errors: any[] = [];
  Object.keys(validationErrors).forEach(key => {
    const props = validationErrors[key].properties;
    const value = props ? (props.value ? props.value : 'missing') : 'missing';
    errors.push({
      name: e.invalidField.code,
      message: e.invalidField.message,
      details: {
        field: key,
        value
      }
    });
  });
  return {
    errors
  };
}

function mongooseErrorOr404(err: any, res: any) {
  if (err.name === 'CastError' && err.kind === 'ObjectId') {
    res.status(404).send();
    return;
  }
  console.error(err);
  res.status(500).send();
}

export { mongooseValidation, mongooseErrorOr404 };
