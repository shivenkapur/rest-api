import { createTestCharitesCampaigns, createDonations } from '../populate';
import { connect, mongoose } from '../../server/config/database';
import { model as CampaignModel } from '../../server/models/campaign';
import { model as CharityModel } from '../../server/models/charity';
import { fakeDonation, fakeCampaign, fakeCharity } from '../data';
import { Donation } from '../../server/models/donation';
import { model as EmailModel } from '../../server/models/email';
import { saveDonorEmailConfirmation } from '../../server/modules/donoremailconfirmation';

beforeAll(() => {
  return connect();
});

beforeEach(() => {
  return mongoose.connection.dropDatabase();
});

afterAll(() => {
  mongoose.disconnect();
});

describe('Donor email confirmation', () => {
  it('exactly one email document with donor email should be created when new donation is posted', done => {
    const charity = new CharityModel(fakeCharity());
    const campaign = new CampaignModel(fakeCampaign({ charity }));
    let donation: Donation;
    charity.save().then(() => {
      campaign
        .save()
        .then(doc => {
          donation = fakeDonation({ campaign: doc });
          return saveDonorEmailConfirmation(campaign, donation);
        })
        .then(() => {
          EmailModel.find({
            recipientAddress: donation.donorDetails.email
          }).then(emails => {
            expect(emails.length).toBe(1);
            done();
          });
        })
        .catch(err => {
          done(err);
        });
    });
  });

  it('email document with correct template data should be created when new donation is posted', done => {
    const charity = new CharityModel(fakeCharity());
    const campaign = new CampaignModel(fakeCampaign({ charity }));
    let donation: Donation;
    charity.save().then(() => {
      campaign
        .save()
        .then(doc => {
          donation = fakeDonation({ campaign: doc });
          return saveDonorEmailConfirmation(campaign, donation);
        })
        .then(() => {
          EmailModel.find({
            recipientAddress: donation.donorDetails.email
          }).then(emails => {
            expect(emails.length).toBe(1);

            const templateData = emails[0].templateData;
            expect(templateData.get('charityName')).toEqual(charity.name);
            expect(templateData.get('donorName')).toEqual(
              donation.donorDetails.name
            );
            expect(templateData.get('campaignTitle')).toEqual(campaign.title);
            expect(templateData.get('campaignNumberDonors')).toEqual(
              (campaign.fundraising.donations + 1).toString()
            );

            done();
          });
        })
        .catch(err => {
          done(err);
        });
    });
  });
});
