import {
  createTestCharitesCampaigns,
  createDonations
} from '../../tests/populate';

import { connect, mongoose } from '../../server/config/database';
import { updateAllDonationTotals } from '../../server/modules/campaigintotals';
import { model as CampaignModel, Campaign } from '../../server/models/campaign';

beforeAll(() => {
  return connect();
});

beforeEach(() => {
  return mongoose.connection.dropDatabase();
});

afterAll(() => {
  return mongoose.disconnect();
});

describe('Campaigin totals', () => {
  it('should update the campaign with totals from the associated donations', done => {
    const numTestDonations = 100;
    let campaignId: string;
    let amountReceived: number = 0;

    createTestCharitesCampaigns(1, 2)
      .then(campaigns => {
        campaignId = campaigns[0][0]._id;
        return Promise.all([
          createDonations(campaigns[0][0], numTestDonations),
          createDonations(campaigns[0][1], numTestDonations)
        ]);
      })
      .then(donations => {
        donations[0].forEach(donation => {
          amountReceived += donation.amount;
        });
        return updateAllDonationTotals();
      })
      .then(() => {
        return CampaignModel.findById(campaignId).exec();
      })
      .then(campaign => {
        if (campaign) {
          expect(campaign.fundraising.donations).toBe(numTestDonations);
          expect(campaign.fundraising.amountRaised).toBe(amountReceived);
          done();
        } else {
          done('campaign not found');
        }
      })
      .catch(err => {
        done(err);
      });
  });
});
