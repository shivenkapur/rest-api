import { connect, mongoose } from '../../server/config/database';
import { model as EmailModel, EmailStatus } from '../../server/models/email';
import { processEmails } from '../../server/modules/emailprocessing';
import { fakeEmail } from '../data';

jest.mock('../../server/modules/email');

beforeAll(() => {
  return connect();
});

beforeEach(() => {
  return mongoose.connection.dropDatabase();
});

afterAll(() => {
  return mongoose.disconnect();
});

function addEmails(
  count: number,
  status: EmailStatus,
  templateName = 'templateName'
) {
  const emails = [];
  let i = count;
  while (i > 0) {
    emails.push(EmailModel.create(fakeEmail({ status, templateName })));
    i--;
  }
  return Promise.all(emails);
}

describe('Email processing', () => {
  it('should select emails in the New state, update to Pending, send the email, then set to Sent', async done => {
    try {
      await addEmails(10, EmailStatus.New);
      const processed = await processEmails();
      expect(processed).toBe(10);

      // check status change
      const sent = await EmailModel.find({ status: EmailStatus.Sent });
      expect(sent.length).toBe(10);
      done();
    } catch (err) {
      done(err);
    }
  });

  it('should ignore emails not in the New state', async done => {
    try {
      await addEmails(4, EmailStatus.New);
      await addEmails(2, EmailStatus.Sent);
      await addEmails(2, EmailStatus.Error);
      await addEmails(2, EmailStatus.Pending);
      const processed = await processEmails();
      expect(processed).toBe(4);

      // check status change
      const sent = await EmailModel.find({ status: EmailStatus.Sent });
      expect(sent.length).toBe(6);
      done();
    } catch (err) {
      done(err);
    }
  });

  it('should leave emails that caused an exception in the Error state', async done => {
    try {
      await addEmails(1, EmailStatus.New, 'error');
      const processed = await processEmails();
      const sent = await EmailModel.find({ status: EmailStatus.Error });
      expect(sent.length).toBe(1);
    } catch (err) {
      done(err);
    }
    done();
  });
});
