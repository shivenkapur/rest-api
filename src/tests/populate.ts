import { model as CharityModel, Charity } from '../server/models/charity';
import { model as CampaignModel, Campaign } from '../server/models/campaign';
import { model as DonationModel, Donation } from '../server/models/donation';

import { fakeCharity, fakeCampaign, fakeDonation } from './data';

function createCharity() {
  const charityDoc = fakeCharity();
  const charity = new CharityModel(charityDoc);
  return charity.save();
}

function createCampaign(charity: Charity): Promise<Campaign> {
  const campaignDoc = fakeCampaign({ charity });
  const campaign = new CampaignModel(campaignDoc);
  return campaign.save();
}

function createCampaigns(charity: Charity, campaignCount: number) {
  const futureCampaigns: Array<Promise<Campaign>> = [];
  for (let i = 0; i < campaignCount; i++) {
    futureCampaigns.push(createCampaign(charity));
  }
  return Promise.all(futureCampaigns);
}

function createCharityCampaigns(
  campaignCount: number,
  charityCallback: (c: Charity) => void
) {
  return createCharity().then(charity => {
    charityCallback(charity);
    return createCampaigns(charity, campaignCount);
  });
}

function createTestCharitesCampaigns(
  charityCount: number,
  campaignCount: number,
  charityCallback: (c: Charity) => void = () => undefined
) {
  const charitiesCampaigns: Array<Promise<Campaign[]>> = [];
  for (let i = 0; i < charityCount; i++) {
    charitiesCampaigns.push(
      createCharity().then(charity => {
        return createCampaigns(charity, campaignCount);
      })
    );
  }
  return Promise.all(charitiesCampaigns);
}

function createDonations(
  campaign: Campaign,
  donationCount: number
): Promise<Donation[]> {
  const donations: Array<Promise<Donation>> = [];
  for (let x = 0; x < donationCount; x++) {
    donations.push(
      DonationModel.create(
        fakeDonation({
          campaign
        })
      )
    );
  }
  return Promise.all(donations);
}

export {
  createCharity,
  createCampaign,
  createCampaigns,
  createDonations,
  createCharityCampaigns,
  createTestCharitesCampaigns
};
