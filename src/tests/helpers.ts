import * as _ from 'lodash';
import { ObjectID } from 'bson';

// makes dates that can be either a Date object or a JSON quoted string rep.
// both quoted strings
function stringyDate(obj: string | Date): string {
  return typeof obj === 'object' ? `${JSON.stringify(obj)}` : `"${obj}"`;
}

// makes IDs than can be either a mongo ObjectId object or a string both a quoted string
function stringyID(val: string | ObjectID) {
  return typeof val === 'object'
    ? `${JSON.stringify(val.toHexString())}`
    : `"${val}"`;
}

function isMatchWithCustom(object: any, source: any) {
  const custom = (left: any, right: any, key: any) => {
    if (_.isDate(left) || _.isDate(right)) {
      return stringyDate(left) === stringyDate(right);
    }
    if (
      (_.hasIn(left, 'constructor.name') &&
        left.constructor.name === 'ObjectID') ||
      (_.hasIn(right, 'constructor.name') &&
        right.constructor.name === 'ObjectID')
    ) {
      return stringyID(left) === stringyID(right);
    }
    // TODO bad TS definition, must return boolean
    // return undefined;

    // _.isMatch semantics
    if (source.key === undefined) {
      return true;
    }
    return _.isEqual(left, right);
  };
  return _.isMatchWith(object, source, custom);
}

/**
 * randomSubString return a substring from text with a random length
 * greater than min and <= length of text, from a random position in
 * text.
 */
function randomSubString(text: string, min: number = 1) {
  if (text.length < min) {
    throw new Error('length of string less than min substring');
  }
  // random is inclusive, substring non-inclusive
  const lower = _.random(text.length - min - 1);
  const upper = _.random(lower + 2, text.length - 1);
  return text.substring(lower, upper + 1);
}

export { isMatchWithCustom, randomSubString };
