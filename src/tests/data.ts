import * as faker from 'faker';

import { Charity } from '../server/models/charity';
import { Photo } from '../server/models/photo';
import { Campaign } from '../server/models/campaign';
import { EmailStatus, Email } from '../server/models/email';
import { MongooseDocument } from 'mongoose';

function fakeCampaignPhoto(): any {
  return {
    fileName: '1e0ad18b-f838-4484-ac2e-94e631741eb2.png'
  };
}

function fakeCharityLogo(): any {
  return {
    fileName: '1e0ad18b-f838-4484-ac2e-94e631741eb1.png'
  };
}

function fakeCampaign({
  mainImage = null,
  charity = null,
  amountRaised = null,
  active = null
}: {
  mainImage?: Photo;
  charity?: Charity;
  amountRaised?: number;
  active?: boolean;
} = {}): any {
  if (!active && active !== false) {
    active = faker.random.boolean();
  }
  const endDate = active ? faker.date.future() : faker.date.past();
  const goalValue = faker.random.number({ min: 1, max: 50000 });
  if (!amountRaised && amountRaised !== 0) {
    amountRaised = faker.random.number({
      min: 1,
      max: goalValue
    });
  }
  // min donation of 1 dollar
  const donations = faker.random.number({ max: amountRaised });

  const images = new Map<string, Photo>();
  images.set('main', mainImage ? mainImage : fakeCampaignPhoto());
  return {
    title: faker.lorem.words(4),
    active,
    endDate,
    details: {
      images,
      summary: faker.lorem.paragraph(),
      description: faker.lorem.paragraphs(3),
      areaAffected: faker.lorem.sentence(),
      peopleAffected: faker.lorem.sentence(),
      helpRequired: faker.lorem.sentence(),
      location: {
        lat: faker.random.number({ min: -90, max: 90 }),
        lng: faker.random.number({ min: -180, max: 180 }),
        zoom: faker.random.number({ min: 10, max: 12 }),
        radius: faker.random.number({ max: 5000 })
      }
    },
    charity: charity ? charity._id.toString() : null,
    fundraising: {
      goal: {
        currency: 'USD',
        value: goalValue
      },
      donations,
      amountRaised
    }
  };
}

function fakeCharity(): any {
  return {
    name: faker.company.companyName(),
    description: faker.lorem.sentence(),
    logo: fakeCharityLogo()
  };
}

function fakeDonation({
  campaign = null
}: {
  campaign?: Campaign;
} = {}): any {
  const amount = faker.random.number({ max: 100 });
  const paymentMethod = faker.random.uuid();
  const email = faker.internet.email();
  const name = faker.name.findName();
  const address = faker.address.streetAddress();
  const country = faker.address.country();
  const paymentId = faker.random.uuid();
  return {
    madeAt: faker.date.past(),
    amount,
    paymentMethod,
    campaign: campaign ? campaign._id.toString() : null,
    paymentId,
    donorDetails: {
      email,
      name,
      address,
      country
    }
  };
}

function fakeEmail({
  status = EmailStatus.New,
  templateName = 'receipt',
  templateData = {}
}: {
  templateData?: any;
  status?: EmailStatus;
  templateName?: string;
}) {
  return {
    recipientAddress: faker.internet.email(),
    recipientName: `${faker.name.firstName()} ${faker.name.lastName()}`,
    templateName,
    templateData,
    status
  };
}

export {
  fakeEmail,
  fakeDonation,
  fakeCampaignPhoto,
  fakeCharityLogo,
  fakeCharity,
  fakeCampaign
};
