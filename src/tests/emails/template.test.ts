import { connect, mongoose } from '../../server/config/database';
import { fakeEmail } from '../data';
import { model as EmailModel } from '../../server/models/email';
import { sendEmail } from '../../server/modules/email';

import * as faker from 'faker';
import * as moment from 'moment';

describe('Email templates', () => {
  it('should recognize the email receipt template', async done => {
    const templateData = {
      donationDate: moment().format('DD/MM/YY'),
      donationAmount: '5',
      donorName: faker.name.firstName,
      donorEmail: faker.internet.email(),
      charityName: faker.company.companyName(),
      campaignGoal: '500',
      campaignTitle: faker.lorem.words(),
      campaignDisasterArea: faker.lorem.words(),
      campaignAmountRaised: '100',
      campaignNumberDonors: '50',
      paymentMethod: faker.lorem.word(),
      paymentID: `${faker.random.number}`,
      donationsRequired: '200'
    };
    const email = fakeEmail({ templateName: 'receipt', templateData });
    sendEmail(email.recipientAddress, email.templateName, email.templateData)
      .then((res: any) => {
        if (!JSON.parse(res.message).html) {
          done('receipt template did not render');
        }
        done();
      })
      .catch((err: any) => done(err));
  });
});
