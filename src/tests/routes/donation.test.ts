import * as request from 'supertest';
import * as query from 'querystring';
import * as url from 'url';
import * as _ from 'lodash';
import * as moment from 'moment';

import { randomSubString, isMatchWithCustom } from '../helpers';
import { createTestCharitesCampaigns, createDonations } from '../populate';

import { app } from '../../server/app';
import { connect, mongoose } from '../../server/config/database';
import * as donoremailconfirmation from '../../server/modules/donoremailconfirmation';
import { model as CampaignModel, Campaign } from '../../server/models/campaign';
import { model as CharityModel } from '../../server/models/charity';
import { fakeDonation, fakeCampaign, fakeCharity } from '../data';
import { Donation } from '../../server/models/donation';
import { model as EmailModel } from '../../server/models/email';
import { ObjectID } from 'bson';
import { Schema } from 'mongoose';

const donationsUrl = '/api/v1/donations/';

function donationsQuery(queryObj: any) {
  const urlQuery = query.stringify(queryObj);
  return `${donationsUrl}?${urlQuery}`;
}

beforeAll(() => {
  return connect();
});

beforeEach(() => {
  return mongoose.connection.dropDatabase();
});

afterAll(() => {
  mongoose.disconnect();
});

jest.mock('../../server/modules/donoremailconfirmation');

describe('Donation endpoint', () => {
  it('should be able to create a donation', (done): void => {
    const charity = new CharityModel(fakeCharity());
    const campaign = new CampaignModel(fakeCampaign({ charity }));
    let donation: Donation;
    charity.save().then(() => {
      campaign
        .save()
        .then(doc => {
          donation = fakeDonation({ campaign: doc });
          return request(app)
            .post(`/api/v1/campaigns/${doc._id}/donations`)
            .set('content-type', 'application/json')
            .send(donation);
        })
        .then((res: request.Response) => {
          expect(res.status).toEqual(201);
          expect(res.body.campaign).toEqual(donation.campaign);
          expect(
            (donoremailconfirmation.saveDonorEmailConfirmation as any).mock
              .calls.length
          ).toBe(1);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });

  async function setupDB() {
    let donations: Donation[];
    let campaigns: Campaign[];

    const testCampaigns = await createTestCharitesCampaigns(1, 20);
    campaigns = _.flatten(testCampaigns);

    const futureDonations: Array<Promise<Donation[]>> = [];
    campaigns.forEach(c => {
      futureDonations.push(createDonations(c, 20));
    });

    donations = _.flatten(await Promise.all(futureDonations));

    return { donations, campaigns };
  }
  it('get donations should respect amount filter', async done => {
    try {
      const { donations, campaigns } = await setupDB();

      const amountMin = 0;
      const amountMax = 50;
      const resp = await request(app).get(
        donationsQuery({ amountMin, amountMax })
      );
      expect(resp.status).toBe(200);
      expect(resp.body.value.length).not.toBe(0);

      const expectedNumber = donations.filter(
        d => d.amount >= amountMin && d.amount <= amountMax
      ).length;
      expect(resp.body.value.length).toBe(expectedNumber);
      done();
    } catch (err) {
      done(err);
    }
  });

  it('get donations should respect end date filter', async done => {
    try {
      const { donations, campaigns } = await setupDB();

      const dateStart = moment().subtract(6, 'months');
      const dateEnd = moment();
      const resp = await request(app).get(
        donationsQuery({
          dateStart: dateStart.unix() * 1000,
          dateEnd: dateEnd.unix() * 1000
        })
      );
      expect(resp.status).toBe(200);
      expect(resp.body.value.length).not.toBe(0);

      const expectedNumber = donations.filter(
        d =>
          moment(d.madeAt).isAfter(dateStart) &&
          moment(d.madeAt).isBefore(dateEnd)
      ).length;
      expect(expectedNumber).toBe(resp.body.value.length);
      done();
    } catch (err) {
      done(err);
    }
  });

  it('get donations should respect search name', async done => {
    try {
      const { donations, campaigns } = await setupDB();
      const searchName = randomSubString(
        donations[_.random(donations.length - 1)].donorDetails.name,
        3
      );
      expect(searchName.length).toBeGreaterThanOrEqual(3);
      const resp = await request(app).get(donationsQuery({ name: searchName }));

      expect(resp.status).toBe(200);
      expect(resp.body.value.length).not.toBe(0);

      const expectedNumber = donations.filter(
        d => d.donorDetails.name.indexOf(searchName) > -1
      ).length;

      expect(expectedNumber).toBe(resp.body.value.length);
      done();
    } catch (e) {
      done(e);
    }
  });

  it('should be able to fetch a donation', async done => {
    const { donations, campaigns } = await setupDB();
    request(app)
      .get(`${donationsUrl}${donations[0]._id}`)
      .set('content-type', 'application/json')
      .send()
      .then(
        (res: any): void => {
          expect(res.status).toEqual(200);
          Reflect.deleteProperty(res.body, 'id');
          expect(isMatchWithCustom(donations[0], res.body)).toBeTruthy();
          done();
        }
      )
      .catch(err => {
        done(err);
      });
  });
});
