import * as request from 'supertest';
import * as _ from 'lodash';

import { fakeCharity } from '../data';
import { connect, mongoose } from '../../server/config/database';
import { app } from '../../server/app';
import { model as CharityModel, Charity } from '../../server/models/charity';

beforeAll(() => {
  return connect();
});

beforeEach(() => {
  return mongoose.connection.dropDatabase();
});

afterAll(() => {
  return mongoose.disconnect();
});

const charityUrl = '/api/v1/charities';

describe('Charity endpoint', () => {
  it('should be able to create a charity', (done): void => {
    const charity = fakeCharity();
    request(app)
      .post(charityUrl)
      .set('content-type', 'application/json')
      .send(charity)
      .then(
        (res: any): void => {
          expect(res.status).toEqual(201);

          expect(res.body.logo.id).not.toBeNull();
          expect(res.body.id).not.toBeNull();
          expect(_.isMatch(res.body, charity)).toBeTruthy();
          done();
        }
      )
      .catch(err => {
        done(err);
      });
  });

  it('should be able to update a charity', (done): void => {
    const charity = fakeCharity();
    let charityId: string;
    const updatedCharity = fakeCharity();
    CharityModel.create(charity)
      .then((doc: any) => {
        charityId = doc._id.toString();
        return request(app)
          .patch(`${charityUrl}/${charityId}`)
          .set('content-type', 'application/json')
          .send(updatedCharity);
      })
      .then(
        (res: any): void => {
          expect(res.status).toEqual(200);
          expect(res.body.id).toEqual(charityId);
          expect(_.isMatch(res.body, updatedCharity)).toBeTruthy();
          done();
        }
      )
      .catch(err => {
        done(err);
      });
  });

  it('should be able to fetch a charity', (done): void => {
    const charity = fakeCharity();
    CharityModel.create(charity)
      .then((doc: any) => {
        return request(app)
          .get(`${charityUrl}/${doc._id}`)
          .set('content-type', 'application/json')
          .send();
      })
      .then(
        (res: any): void => {
          expect(res.status).toEqual(200);
          expect(_.isMatch(res.body, charity)).toBeTruthy();
          done();
        }
      )
      .catch(err => {
        done(err);
      });
  });

  it('should be able to get charities', (done): void => {
    const numCharities = 5;
    const addRequests = [];
    const charities: Charity[] = [];
    for (let i = 0; i < numCharities; i++) {
      const charity = fakeCharity();
      charities.push(charity);
      addRequests.push(CharityModel.create(charity));
    }
    Promise.all(addRequests)
      .then((docs: any[]) => {
        return request(app).get(charityUrl);
      })
      .then(res => {
        expect(res.status).toEqual(200);
        res.body.forEach((doc: Charity) => {
          let match = false;
          charities.forEach(source => {
            if (_.isMatch(doc, source)) {
              match = true;
            }
          });
          expect(match).toBeTruthy();
        });
        done();
      })
      .catch(err => {
        done(err);
      });
  });
});
