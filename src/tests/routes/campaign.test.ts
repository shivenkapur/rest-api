import * as request from 'supertest';
import * as _ from 'lodash';
import { resolve } from 'url';
import * as query from 'querystring';
import { connect, mongoose } from '../../server/config/database';
import { app } from '../../server/app';
import { model as CharityModel, Charity } from '../../server/models/charity';
import { model as CampaignModel, Campaign } from '../../server/models/campaign';
import { model as DonationModel } from '../../server/models/donation';
import { fakeCampaign, fakeCharity, fakeDonation } from '../data';
import { isMatchWithCustom } from '../helpers';
import { createTestCharitesCampaigns } from '../populate';

const campaignUrl = '/api/v1/campaigns/';

function campaignQuery(queryObj: any) {
  const urlQuery = query.stringify(queryObj);
  return `${campaignUrl}?${urlQuery}`;
}

function randomSubString(text: string, min: number = 1) {
  if (text.length < min) {
    throw new Error('length of string less than min substring');
  }
  const lower = _.random(text.length - min);
  const upper = _.random(lower, text.length);
  return text.substring(lower, upper);
}

function saveCharityFakeCampaign(charity: Charity): Promise<Campaign> {
  return charity.save().then(model => {
    return addCampaign(charity);
  });
}

function addCampaign(charity: Charity): Promise<Campaign> {
  const campaignDoc = fakeCampaign({ charity });
  const campaign = new CampaignModel(campaignDoc);
  return campaign.save();
}

beforeAll(() => {
  return connect();
});

beforeEach(() => {
  return mongoose.connection.dropDatabase();
});

afterAll(() => {
  return mongoose.disconnect();
});

describe('Campaign endpoints', () => {
  it('should be able to create a campaign', (done): void => {
    const charityDoc = fakeCharity();
    const charity = new CharityModel(charityDoc);
    let campaign: Campaign;
    charity
      .save()
      .then(doc => {
        campaign = fakeCampaign({ charity: doc });
        return request(app)
          .post(campaignUrl)
          .set('content-type', 'application/json')
          .send(campaign);
      })
      .then((res: request.Response) => {
        expect(res.status).toEqual(201);
        expect(isMatchWithCustom(res.body, campaign)).toBeTruthy();
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('get campaigns should respect filter and search parameters', async done => {
    try {
      let campaigns: Campaign[];

      const testCampaigns = await createTestCharitesCampaigns(5, 20);
      campaigns = _.flatten(testCampaigns);

      // test active parameter
      let resp = await request(app).get(campaignQuery({ inactive: false }));
      const numActive = campaigns.filter(campaign => campaign.active).length;
      expect(resp.body.items.length).toBe(numActive);

      resp = await request(app).get(campaignQuery({ inactive: true }));
      expect(resp.body.items.length).toBe(campaigns.length);

      // test title search string parameter
      const searchTitle = randomSubString(
        campaigns[_.random(campaigns.length - 1)].title
      );
      resp = await request(app).get(campaignQuery({ criteria: searchTitle }));

      const filteredCampaigns = campaigns.filter(
        c => c.title.indexOf(searchTitle) > -1 && c.active
      );
      expect(filteredCampaigns.length).toBe(resp.body.items.length);
      done();
    } catch (e) {
      done(e);
    }
  });

  it('should be able to get campaigns', (done): void => {
    let campaignModels: Campaign[];
    const futureCampaigns: Array<Promise<Campaign>> = [];
    const charity = new CharityModel(fakeCharity());
    for (let i = 0; i < 5; i++) {
      futureCampaigns.push(addCampaign(charity));
    }
    charity
      .save()
      .then(model => {
        return Promise.all(futureCampaigns);
      })
      .then((campaigns: Campaign[]) => {
        campaignModels = campaigns;
        return request(app).get(campaignUrl);
      })
      .then((res: request.Response) => {
        expect(res.status).toEqual(200);
        expect(res.body.items.length).toEqual(
          campaignModels.filter(c => c.active).length
        );
        campaignModels.forEach(model => {
          expect(_.isMatch(res.body, model));
        });
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('should be able to get a campaign', (done): void => {
    let campaign: Campaign;
    const charity = new CharityModel(fakeCharity());
    charity
      .save()
      .then(model => {
        return addCampaign(charity);
      })
      .then(model => {
        campaign = model._doc;
        return request(app).get(
          resolve(campaignUrl, `${model._id.toString()}`)
        );
      })
      .then(res => {
        expect(res.status).toEqual(200);
        // TODO abstract this model to JSON reponse comparison
        delete campaign._id;
        delete campaign.__v;
        res.body.charity = res.body.charity.id;
        campaign.charity = campaign.charity.toString();
        expect(isMatchWithCustom(res.body, campaign)).toBeTruthy();
        done();
      })
      .catch(err => {
        done(err);
      });
  });

  it('should be able to update a campaign', (done): void => {
    let campaignId: string;
    const charity = fakeCharity();
    const charityDoc = new CharityModel(charity);
    const updatedCampaign = fakeCampaign({ charity: charityDoc });
    saveCharityFakeCampaign(charityDoc)
      .then(doc => {
        campaignId = doc._id.toString();
        return request(app)
          .put(resolve(campaignUrl, `${campaignId}`))
          .set('content-type', 'application/json')
          .send(updatedCampaign);
      })
      .then(
        (res: any): void => {
          expect(res.status).toEqual(200);
          expect(res.body.id).toEqual(campaignId);
          expect(isMatchWithCustom(res.body, updatedCampaign)).toBeTruthy();
          console.log(res.body);
          done();
        }
      )
      .catch(err => {
        done(err);
      });
  });
  describe('delete campaign', () => {
    it('should be able to delete an inactive campaign with 0 amount raised', async (): Promise<
      any
    > => {
      const charity = await new CharityModel(fakeCharity()).save();
      const campaign = await new CampaignModel(
        fakeCampaign({
          active: false,
          amountRaised: 0,
          charity
        })
      ).save();

      const res = await request(app).delete(
        resolve(campaignUrl, campaign._id.toString())
      );

      expect(res.status).toEqual(202);
      expect(await CampaignModel.findById(campaign._id.toString())).toBeNull();
    });

    it('should NOT be able to delete an active campaign', async (): Promise<
      any
    > => {
      const charity = await new CharityModel(fakeCharity()).save();
      const campaign = fakeCampaign({
        active: true,
        charity
      });
      const campaignModel = await new CampaignModel(campaign).save();

      const res = await request(app).delete(
        resolve(campaignUrl, campaignModel._id.toString())
      );

      expect(res.status).toEqual(400);
      expect(
        await CampaignModel.findById(campaignModel._id.toString())
      ).toBeDefined();
    });

    it('should NOT be able to delete an inactive campaign with donations', async (): Promise<
      any
    > => {
      const charity = await new CharityModel(fakeCharity()).save();
      const campaign = fakeCampaign({
        active: false,
        charity
      });
      const campaignModel = await new CampaignModel(campaign).save();

      await new DonationModel(fakeDonation({ campaign: campaignModel })).save();
      await new DonationModel(fakeDonation({ campaign: campaignModel })).save();

      const res = await request(app).delete(
        resolve(campaignUrl, campaignModel._id.toString())
      );

      expect(res.status).toEqual(400);
      expect(
        await CampaignModel.findById(campaignModel._id.toString())
      ).toBeDefined();
    });
  });
});
