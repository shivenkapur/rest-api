import * as request from 'supertest';
import * as fs from 'fs';
import * as path from 'path';

import { connect, mongoose } from '../../server/config/database';
import { imagePath } from '../../server/modules/localstorage';
import { app } from '../../server/app';

const urlPhotos = '/api/v1/photos';
const imgPath = path.join(__dirname, '../assets/aah_logo.png');
const txtPath = path.join(__dirname, '../assets/text.txt');

beforeAll(() => {
  return connect();
});

afterAll(() => {
  return mongoose.disconnect();
});

describe('Photos endpoint', () => {
  it('should be able to add a photo', (done): void => {
    request(app)
      .post(urlPhotos)
      .set('content-type', 'application/octet-stream')
      .send(fs.readFileSync(imgPath))
      .end(
        (err: Error, res: any): void => {
          if (err) {
            return done(err);
          }
          expect(res.status).toEqual(201);
          expect(
            fs.existsSync(path.join(imagePath, res.body.fileName))
          ).toBeTruthy();
          done();
        }
      );
  });

  // TODO
  // it('should be able to delete a photo', (done): void => {
  //   request(app)
  //     .post(urlPhotos)
  //     .set('content-type', 'application/octet-stream')
  //     .send(fs.readFileSync(imgPath))
  //     .end((err: Error, res: any): void => {
  //       if (err) {
  //         return done(err);
  //       }
  //       expect(res.status).toEqual(201);
  //       expect(
  //         fs.existsSync(path.join(imagePath, res.body.fileName))
  //       ).toBeTruthy();
  //       done();
  //     });
  // });

  it('only image files accepted', (done): void => {
    request(app)
      .post(urlPhotos)
      .set('content-type', 'application/octet-stream')
      .send(fs.readFileSync(txtPath))
      .end(
        (err: Error, res: any): void => {
          if (err) {
            return done(err);
          }
          expect(res.status).toEqual(400);
          done();
        }
      );
  });
});
