const NodeEnvironment = require('jest-environment-node');

class OneReliefEnvironment extends NodeEnvironment {
  constructor(config) {
    super(config);
  }

  async setup() {
    console.log('Setup Test Environment');
    this.global.__MONGO__ = global.__MONGO__;
    await super.setup();
  }

  async teardown() {
    console.log('Teardown Test Environment');
    await super.teardown();
  }

  runScript(script) {
    return super.runScript(script);
  }
}

module.exports = OneReliefEnvironment;
