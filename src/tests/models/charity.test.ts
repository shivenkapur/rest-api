import * as _ from 'lodash';

import { connect, mongoose } from '../../server/config/database';
import { model as Charity } from '../../server/models/charity';
import { fakeCharity } from '../data';

beforeAll(() => {
  return connect();
});

afterAll(() => {
  return mongoose.disconnect();
});

describe('Charity model', () => {
  it('should insert new Charity', async done => {
    try {
      const charityDoc = fakeCharity();
      const charity = new Charity(charityDoc);
      const savedCharity = await charity.save();

      expect(_.isMatch(savedCharity, charityDoc)).toBeTruthy();
      done();
    } catch (e) {
      done(e);
    }
  });

  it('should update Charity', async done => {
    try {
      const charityDoc = fakeCharity();
      const charity = new Charity(charityDoc);

      const savedCharity = await charity.save();
      const fakedCharity: any = fakeCharity();
      const updateResult = await Charity.update(
        { _id: savedCharity._id },
        fakedCharity
      );

      expect(_.isMatch(updateResult, { n: 1, ok: 1 })).toBeTruthy();
      done();
    } catch (e) {
      done(e);
    }
  });
});
