import * as _ from 'lodash';

import { connect, mongoose } from '../../server/config/database';
import { model as Donation } from '../../server/models/donation';
import { model as Charity } from '../../server/models/charity';
import { model as Campaign } from '../../server/models/campaign';
import * as data from '../data';

beforeAll(() => {
  return connect();
});

afterAll(() => {
  return mongoose.disconnect();
});

describe('Donation model', () => {
  it('should store and retrieve all Donation fields', async done => {
    const charityDoc = data.fakeCharity();
    const charity = new Charity(charityDoc);

    try {
      await charity.save();
    } catch (e) {
      done(e);
    }
    const campaignDoc = data.fakeCampaign({ charity });
    const campaign = new Campaign(campaignDoc);

    try {
      await campaign.save();
    } catch (e) {
      done(e);
    }

    const donationDoc = data.fakeDonation({ campaign });
    const donation = new Donation(donationDoc);

    let model;

    try {
      model = await donation.save();
      model = await Donation.findOne({ _id: model._id });
    } catch (e) {
      done(e);
    }

    _.isEqual(model, donationDoc);
    done();
  });
});
