import * as _ from 'lodash';

import { connect, mongoose } from '../../server/config/database';
import { model as CampaignModel, Campaign } from '../../server/models/campaign';
import { model as CharityModel } from '../../server/models/charity';
import * as data from '../data';

beforeEach(() => {
  return mongoose.connection.dropDatabase();
});

beforeAll(() => {
  return connect();
});

afterAll(() => {
  return mongoose.disconnect();
});

describe('Campaign model', () => {
  it('should store and retrieve all Campaign fields', async done => {
    const charityDoc = data.fakeCharity();
    const charity = new CharityModel(charityDoc);

    try {
      await charity.save();
    } catch (e) {
      done(e);
    }
    const campaignDoc = data.fakeCampaign({ charity });
    const campaign = new CampaignModel(campaignDoc);

    let model: Campaign;
    try {
      model = await campaign.save();
    } catch (e) {
      done(e);
    }
    campaignDoc.charity = model!.charity;
    expect(_.isMatch(model!, campaignDoc)).toBeTruthy();
    done();
  });
});
