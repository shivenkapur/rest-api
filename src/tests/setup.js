require('dotenv').config();
const MongodbMemoryServer = require('mongodb-memory-server');

const MONGO_DB_NAME = 'jest';
const mongo = new MongodbMemoryServer.default({
  instance: {
    dbName: MONGO_DB_NAME
  },
  binary: {
    version: '3.4.4',
    downloadDir: '.mongodb-binaries'
  }
});

module.exports = async function() {
  global.__MONGO__ = mongo;
  global.__MONGO_DB_NAME__ = MONGO_DB_NAME;
  // Make synchronous so that MongoDB will download before tests start
  global.__MONGO_URL__ = await mongo.getConnectionString();
};
