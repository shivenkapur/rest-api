import '../server/config/dotenv';

import { connect, mongoose } from '../server/config/database';

connect()
  .then(() => {
    return mongoose.connection.dropDatabase();
  })
  .then(() => {
    console.log('DB cleaned successfully');
    mongoose.connection.close();
  })
  .catch(err => {
    console.error(err);
  });
