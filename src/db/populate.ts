import '../server/config/dotenv';
import { mongoose, connect } from '../server/config/database';
import { model as photoModel, Photo } from '../server/models/photo';
import { model as charityModel, Charity } from '../server/models/charity';
import { model as campaignModel, Campaign } from '../server/models/campaign';
import { model as donationModel, Donation } from '../server/models/donation';

import {
  fakeCampaignPhoto,
  fakeCharity,
  fakeCampaign,
  fakeCharityLogo,
  fakeDonation
} from '../tests/data';

const numberCharities = 2;
const numberCampaigns = 3;
const numberImages = 10;
const numberDonations = 100;

function populateImages(): Promise<Photo[]> {
  const images: Array<Promise<Photo>> = [];
  for (let x = 0; x < numberImages; x++) {
    images.push(photoModel.create(fakeCharityLogo()));
  }
  return Promise.all(images);
}

function populateCharity(): Promise<Charity> {
  return photoModel.create(fakeCharityLogo()).then(model => {
    return charityModel.create(fakeCharity());
  });
}

function populateCampaigns(charity: Charity): Promise<Campaign[]> {
  return photoModel.create(fakeCampaignPhoto()).then(photo => {
    const campaigns: Array<Promise<Campaign>> = [];
    for (let x = 0; x < numberCampaigns; x++) {
      campaigns.push(
        campaignModel.create(
          fakeCampaign({
            mainImage: new photoModel({
              fileName: photo.fileName,
              id: photo.id
            }),
            charity
          })
        )
      );
    }
    return Promise.all(campaigns);
  });
}

function populateDonations(campaign: Campaign): Promise<Donation[]> {
  const donations: Array<Promise<Donation>> = [];
  for (let x = 0; x < numberDonations; x++) {
    donations.push(
      donationModel.create(
        fakeDonation({
          campaign
        })
      )
    );
  }
  return Promise.all(donations);
}

function populate(): Promise<any> {
  const campaigns: Array<Promise<Campaign[]>> = [];

  for (let x = 0; x < numberCharities; x++) {
    const charity = populateCharity();
    campaigns.push(
      charity
        .then(doc => {
          return populateCampaigns(doc);
        })
        .then(campaignModels => {
          campaignModels.map(model => {
            return populateDonations(model);
          });
          return campaignModels;
        })
    );
  }
  return Promise.all<any>([populateImages(), ...campaigns]);
}

connect()
  .then(() => {
    return mongoose.connection.db.collections();
  })
  .then(collections => {
    // Bail out if the database is not empty
    if (collections.length !== 0) {
      throw new Error(`DB '${mongoose.connection.db.databaseName}' not empty`);
    }
    return populate();
  })
  .then(() => {
    console.log('DB populated');
    mongoose.connection.close();
  })
  .catch(e => {
    console.error('unable to populate DB', e);
    mongoose.connection.close();
  });
