#!/bin/bash

LOCAL_IMAGE_NAME=rest-api:latest
IMAGE_URL=$1

ECS_LOGIN=`aws ecr get-login --no-include-email --region us-east-1` && eval $ECS_LOGIN
docker tag "$LOCAL_IMAGE_NAME" "${IMAGE_URL}"
docker push "${IMAGE_URL}"
